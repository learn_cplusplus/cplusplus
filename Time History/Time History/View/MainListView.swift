//
//  MainListView.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

struct MainListView: View {
    var course: Course = courses[0]
    
    var body: some View {
        ZStack(alignment: .top) {
            Image(course.image)
                .resizable()
                .scaledToFit()
                .cornerRadius(10)
                .frame(height: 300)
            Text(course.title)
                .font(.largeTitle)
                .fontWeight(.bold)
                .padding(.top)
                .foregroundStyle(.linearGradient(colors: [.white.opacity(0.6),.white.opacity(0.1)], startPoint: .topLeading, endPoint: .bottomTrailing))
        }
        .padding(10)
    }
}

struct MainListView_Previews: PreviewProvider {
    static var previews: some View {
        MainListView()
    }
}
