//
//  NavigationBar.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

struct NavigationBar: View {
    //  navbar 的标题
    var title = ""
    @Binding var hasScrolled: Bool
    var body: some View {
        ZStack {
            Color.clear
                .background(.ultraThinMaterial)
                .blur(radius: 10) //添加模糊
                .opacity(hasScrolled ? 1 : 0)
            Text(title)
                .animationFont(size: hasScrolled ? 22 : 34, weight: .bold)
                .frame(maxWidth: .infinity, alignment: hasScrolled ? .center : .leading)
                .padding(.leading,20)
//                .padding(.top, 20)
                .offset(y: hasScrolled ? -4 : 0)

//            HStack {
//                Image(systemName: "house")
//                    .font(.body.weight(.bold))
//                    .frame(width: 36,height: 36)
//                    .background(.ultraThinMaterial,in:
//                                RoundedRectangle(cornerRadius: 14, style: .continuous))
//                    .strokeStyle(cornerRadius: 14)
//                
//                Image(systemName: "person")
//                    .font(.body.weight(.bold))
//                    .frame(width: 26,height: 26)
//                    .padding(8)
//                    .cornerRadius(10)
//                    .background(.ultraThinMaterial,in:
//                                RoundedRectangle(cornerRadius: 14, style: .continuous))
//                    .strokeStyle(cornerRadius: 18)
//            }.frame(maxWidth: .infinity, alignment: .trailing)
//                .padding(.trailing, 10)
//                .padding(.top, 20)
//                .offset(y: hasScrolled ? -4 : 0)
        }
        .frame(height: hasScrolled ? 44 : 70)
        .frame(maxHeight: .infinity,alignment: .top)
    }
}

struct NavigationBar_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBar(title: "Time Chart",hasScrolled: .constant(false))
    }
}
