//
//  MainSubView.swift
//  Time History
//
//  Created by 落问 on 2022/10/25.
//

import SwiftUI

struct MainSubView: View {
    var body: some View {
            ZStack(alignment: .top) {
                Image("Illustration 6")
                    .resizable(resizingMode: .stretch)
                    .background(Color.red)
                    .cornerRadius(20)
            }
            .aspectRatio(1, contentMode: .fit)
    }
}

struct MainSubView_Previews: PreviewProvider {
    static var previews: some View {
        MainSubView()
    }
}
