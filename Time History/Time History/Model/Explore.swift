//
//  Explore.swift
//  Time History
//
//  Created by 落问 on 2022/10/24.
//

import SwiftUI

struct Explore: Identifiable {
    var id = UUID()
    var image: String
    var title: String
    var subTitle: String
    var content: String
}

let explores = [
    Explore(image: "Illustration 1", title: "Word", subTitle: "", content: "")
]

