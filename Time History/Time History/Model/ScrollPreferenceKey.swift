//
//  ScrollPreferenceKey.swift
//  Time History
//
//  Created by 落问 on 2022/10/12.
//

import SwiftUI

struct ScrollPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}

