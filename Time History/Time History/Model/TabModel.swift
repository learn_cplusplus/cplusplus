//
//  TabModel.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

struct TabItem: Identifiable {
    // 所有的循环的模型需要一个id
    var id = UUID()
    // tabbar 标题
    var text: String
    // tabbar icon
    var icon: String
    var tab: Tab
    var color: Color
}

var tabItems = [
    TabItem(text: "TimeChart", icon: "house", tab: .timeChart, color: .teal),
    TabItem(text: "Explore", icon: "magnifyingglass", tab: .explore, color: .blue),
    TabItem(text: "Noticications", icon: "bell", tab: .notice, color: .red),
    TabItem(text: "Library", icon: "rectangle.stack", tab: .library, color: .pink)
]

public enum Tab: String {
    case timeChart
    case explore
    case notice
    case library
}

struct TabPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}
