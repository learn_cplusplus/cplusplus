//
//  Course.swift
//  Time History
//
//  Created by 落问 on 2022/10/12.
//

import SwiftUI

struct Course: Identifiable {
    let id = UUID()
    var image: String
    var title: String
}

var courses = [
    Course(image: "Background 1", title: "时间都去哪了"),
    Course(image: "Background 2",title: "明天一定好好学"),
    Course(image: "Background 3",title: "不赢一把不睡觉"),
    Course(image: "Background 4",title: "时间还久慢慢来"),
    Course(image: "Background 5",title: "佳人有约一定到"),
    Course(image: "Background 6",title: "兄弟聚会不缺席"),
    Course(image: "Background 7",title: "考研时间还很久"),
    Course(image: "Background 8",title: "学习时间还很多"),
    Course(image: "Background 9",title: "不知不觉过去了"),
    Course(image: "Background 10",title: "一无所有空悲切")
]
