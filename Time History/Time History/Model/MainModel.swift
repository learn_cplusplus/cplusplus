//
//  MainModel.swift
//  Time History
//
//  Created by 落问 on 2022/10/25.
//

import SwiftUI

struct MainModel: Identifiable {
    var id = UUID()
    var title: String
    var imageName: String    
}
