//
//  MainView.swift
//  Time History
//
//  Created by 落问 on 2022/10/25.
//  首页视图

import SwiftUI

struct MainView: View {
    
    let startAngle = -90.0
    @State var progress = 0.0
    var body: some View {
        GeometryReader { proxy in
            VStack {
                ZStack {
                    Circle()
                        .stroke(Color.purple, style: StrokeStyle(lineWidth: 10))
                    // 内环
                    RingShape(progress: Double(progress), thickness: 10)
                        .fill(AngularGradient(gradient: Gradient(colors: [.red, .green]), center: .center, startAngle: .degrees(-90.0), endAngle: .degrees(360*0.3 + startAngle)))
                        .animation(Animation.easeInOut(duration: 2), value: progress)
                
                }
                .frame(width: 300, height: 300, alignment: .center)
                .padding(20)


                VStack() {
                    HStack(alignment: .center) {
                        MainSubView()
                        MainSubView()
                    }
                    .padding(.horizontal)
                    HStack {
                        MainSubView()
                        MainSubView()
                    }
                    .padding(.horizontal)
                }
            }
            .onAppear {
                // 延迟1后显示当前进度数据
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    progress = 0.5
                })
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}


// 内环
struct RingShape: Shape {
    var progress: Double = 0.0
    var thickness: CGFloat = 10.0
    var startAngle: Double = -90.0
    var animatableData: Double {
        get { progress }
        set { progress = newValue }
    }
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.addArc(center: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0), radius: min(rect.width, rect.height) / 2.0,startAngle: .degrees(startAngle),endAngle: .degrees(360 * progress + startAngle), clockwise: false)
        return path.strokedPath(.init(lineWidth: thickness, lineCap: .round))
    }
}
