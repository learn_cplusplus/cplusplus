//
//  ContentView.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

struct ContentView: View {

    // 这种绑定就算app不在前台，也会记录最后的状态
    @AppStorage("selectedTab") var selectTab: Tab = .timeChart

    @EnvironmentObject var model: Model

    var body: some View {
        ZStack(alignment: .bottom) {
            Group {
                switch selectTab {
                case .timeChart:
                    TimeChartView()
                case .explore:
                    ExploreView()
                case .notice:
                    MainView()
                case .library:
                    LibraryView()
                }
            }.frame(maxWidth: .infinity, maxHeight: .infinity)
            Tabbar()
                .offset(y: model.showDetail ? 200 : 0)
        }.safeAreaInset(edge: .bottom) {
            Color.clear.frame(height: 44)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//struct ContentView: View {
//    var thickness: CGFloat = 30.0
//    var width: CGFloat = 250.0
//    var startAngle = -90.0
//    @State var progress = 0.0
//    var body: some View {
//        VStack {
//            ZStack {
//                // 外环
//                Circle()
//                    .stroke(Color(.systemGray6), lineWidth: thickness)
//                // 内环
//                RingShape(progress: progress, thickness: thickness)
//                    .fill(AngularGradient(gradient: Gradient(colors: [.red, .green]), center: .center, startAngle: .degrees(startAngle), endAngle: .degrees(360*0.3 + startAngle)))
//            }
//            .frame(width: width, height: width, alignment: .center)
//            .animation(Animation.easeInOut(duration: 1.0),value: progress)
//            //进度调节
//            HStack {
//                Group {
//                    Text("0%")
//                        .font(.system(.headline, design: .rounded))
//                        .onTapGesture {
//                            self.progress = 0.0
//                        }
//                    Text("50%")
//                        .font(.system(.headline, design: .rounded))
//                        .onTapGesture {
//                            self.progress = 0.5
//                        }
//                    Text("100%")
//                        .font(.system(.headline, design: .rounded))
//                        .onTapGesture {
//                            self.progress = 1.0
//                        }
//                }
//                .padding()
//                .background(Color(.systemGray6)).clipShape(RoundedRectangle(cornerRadius: 15.0, style: .continuous))
//                .padding()
//            }
//            .padding()
//        }
//    }
//}
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
//// 内环
//struct RingShape: Shape {
//    var progress: Double = 0.0
//    var thickness: CGFloat = 30.0
//    var startAngle: Double = -90.0
//    var animatableData: Double {
//        get { progress }
//        set { progress = newValue }
//    }
//    func path(in rect: CGRect) -> Path {
//        var path = Path()
//        path.addArc(center: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0), radius: min(rect.width, rect.height) / 2.0,startAngle: .degrees(startAngle),endAngle: .degrees(360 * progress + startAngle), clockwise: false)
//        return path.strokedPath(.init(lineWidth: thickness, lineCap: .round))
//    }
//}
