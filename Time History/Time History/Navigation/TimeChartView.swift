//
//  TimeChartView.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

struct TimeChartView: View {
    
    @State var hasScrolled = false
    
    @State var statusBarHidden = false
    
    @EnvironmentObject var model: Model
    
    var body: some View {
        ZStack {
            Color(.white).ignoresSafeArea()
            ScrollView {
                scrollDetection
                featured
                // 设置滚动条 (这个值相当于最大)
                Color.clear.frame(height: 800)
            }
            .coordinateSpace(name: "scroll") // 和上main的代码配合可以获取到contentofset
            .safeAreaInset(edge: .top, content: {
                // 相当于自己创建了一个区域
                Color.clear.frame(height: 70)
            })
            .overlay(
                NavigationBar(title: "TimeChart",hasScrolled: $hasScrolled)
        )
        }
        .statusBarHidden(statusBarHidden)
    }
    
    var scrollDetection: some View {
        GeometryReader { proxy in
            // 获取区域的位置
            Color.clear.preference(key: ScrollPreferenceKey.self, value: proxy.frame(in: .named("scroll")).minY)
        }
        .frame(height: 0)
        .onPreferenceChange(ScrollPreferenceKey.self, perform: { value in
                withAnimation(.easeInOut) {
                    print("当前value:\(value)")
                    if value < -70.0 {
                        hasScrolled = true
                    } else {
                        hasScrolled = false
                    }
                }
            })
    }
    var featured: some View {
        TabView {
            ForEach(courses) { item in
                GeometryReader { proxy in
                    let minx = proxy.frame(in: .global).minX
                    MainListView(course: item)
                        .padding(.vertical,0)
                        .rotation3DEffect(.degrees(minx / -10), axis: (x: 0, y: 1, z: 0))
                        .blur(radius: abs(minx / 10))
                }.onTapGesture {
                    withAnimation(.spring(response: 0.6, dampingFraction: 0.8)) {
                        statusBarHidden = !statusBarHidden
                        model.showDetail.toggle()
                    }
                }
            }
        }
        .tabViewStyle(.page(indexDisplayMode: .never))
        .frame(height: 300)
        .background {
            Image("Background 10").offset(x:250, y: -100)
        }
    }
}

struct TimeChartView_Previews: PreviewProvider {
    static var previews: some View {
        TimeChartView()
            .environmentObject(Model())
    }
}
