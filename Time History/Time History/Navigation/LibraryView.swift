//
//  LibraryView.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/30.
//

import SwiftUI
import ManagedSettings
import FamilyControls
import DeviceActivity

struct LibraryView: View {
    
//    // 活动中心
//    let activityCenter = DeviceActivityCenter()
    
    
    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        let components = DateComponents(second: 15)
        let futureDate = Calendar.current.date(byAdding: components, to: Date())!

        Text(futureDate, style: .timer)
    }
}

struct LibraryView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryView()
    }
}
