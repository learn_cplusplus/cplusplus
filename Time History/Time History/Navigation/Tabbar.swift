//
//  Tabbar.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//  系统的使用TabView

import SwiftUI

struct Tabbar: View {
    // 选中的项目 （这种存储方式相当于userdefaults）
    @AppStorage("selectedTab") var selectTab: Tab = .timeChart
    @State var color: Color = .teal
    @State var tabItemWidth: CGFloat = 0
    
    var body: some View {
        ZStack(alignment: .bottom) {
            HStack {
                Spacer()
                buttons
            }
            .padding(.horizontal,8)
            .padding(.top,14)
            .frame(height: 88, alignment: .top)
            .background(.ultraThinMaterial,in:RoundedRectangle(cornerRadius: 34,style: .continuous))
            .background(
                background
            )
            .overlay(
                overlay
            )
            .strokeStyle(cornerRadius: 34)
            // 设置视图居于最底部
            .frame(maxHeight: .infinity, alignment: .bottom)
            .ignoresSafeArea()
        }
    }
    
    // 按钮视图
    var buttons: some View {
        ForEach(tabItems) { item in
            Button(action: {
                withAnimation(.spring(response: 0.3, dampingFraction: 0.5)) {
                    selectTab = item.tab
                    color = item.color
                }
            }, label:{
                VStack(spacing: 0){
                    Image(systemName: item.icon)
                        .symbolVariant(.fill)
                        .font(.body.bold())
                        .frame(width: 44, height: 29)
                    Text(item.text)
                        .font(.caption2)
                        .lineLimit(1)
                }
                .frame(maxWidth: .infinity)
            })
            .foregroundStyle(selectTab == item.tab ? .primary : .secondary)
            .blendMode(selectTab == item.tab ? .overlay : .normal)
            .overlay(
                GeometryReader { proxy in
                    // 保存值并传递
                    Color.clear.preference(key: TabPreferenceKey.self, value: proxy.size.width)
                }
            )
            .onPreferenceChange(TabPreferenceKey.self) { value in
                tabItemWidth = value
            }
        }
    }
    
    // 背景视图
    var background: some View {
        HStack {
            if selectTab == .library || selectTab == .explore {Spacer()}
            if selectTab == .notice {
                Spacer()
                Spacer()
            }
            Circle().fill(color).frame(width:tabItemWidth)
            if selectTab == .timeChart || selectTab == .notice  {Spacer()}
            if selectTab == .explore {
                Spacer()
                Spacer()
            }
        }
        .padding(.horizontal, 8)
    }
    
    var overlay: some View {
        HStack {
            if selectTab == .library || selectTab == .explore {Spacer()}
            if selectTab == .notice {
                Spacer()
                Spacer()
            }
            Rectangle()
                .fill(color)
                .frame(width:30, height:5)
                .cornerRadius(3)
                .frame(width: tabItemWidth)
                .frame(maxHeight: .infinity, alignment: .top)
            if selectTab == .timeChart || selectTab == .notice  {Spacer()}
            if selectTab == .explore {
                Spacer()
                Spacer()
            }
        }
        .padding(.horizontal, 8)

    }
}

struct Tabbar_Previews: PreviewProvider {
    static var previews: some View {
        Tabbar()
    }
}
