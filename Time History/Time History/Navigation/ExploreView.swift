//
//  ExploreView.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/14.
//

import SwiftUI

struct ExploreView: View {
    var body: some View {
        ScrollView {
            ForEach(0 ..< 5) {_ in
                MatchedView()
            }
        }
        .background(Color.red)
    }
}

struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}
