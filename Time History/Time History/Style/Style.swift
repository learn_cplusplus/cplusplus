//
//  Style.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//  通用视图填充类型

import SwiftUI

struct FillStrokeStyle: ViewModifier {
    var cornerRadius: CGFloat
    // 是否是暗黑模式
    @Environment(\.colorScheme) var colorScheme
    func body(content: Content) -> some View {
        content.overlay(
                RoundedRectangle(cornerRadius: cornerRadius, style: .continuous)
                    .stroke(.linearGradient(colors: [.white.opacity(colorScheme == .dark ? 0.6 : 0.3),.black.opacity(colorScheme == .dark ? 0.3 : 0.1)], startPoint: .top, endPoint: .bottom))
                    .blendMode(.overlay)
        )
    }
}

extension View {
    func strokeStyle(cornerRadius: CGFloat = 30) ->  some View {
        modifier(FillStrokeStyle(cornerRadius: cornerRadius))
    }
}
