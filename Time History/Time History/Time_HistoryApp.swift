//
//  Time_HistoryApp.swift
//  Time History
//
//  Created by Macbook-iOS on 2022/10/12.
//

import SwiftUI

@main
struct Time_HistoryApp: App {
    // 保证只有一个
    @StateObject var model = Model()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(model)
        }
    }
}
